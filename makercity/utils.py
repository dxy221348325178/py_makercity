import time
import math
from django.shortcuts import render #导入render 方法
from django.http import JsonResponse
import json
# 模板处理函数
def dispose_debug_data(request):
    data = {
        'va':'?v='+str(int(time.time())),
    }
    return data
# 公共变量
def globel_params(request):
    data = {
        'v':'?v='+str(int(time.time())),
    }
    return data
# 自定义输出方法
def my_render(request, template, context={}):
    if(context['debug']):
        data = json.dumps(context, indent=4, ensure_ascii=False, sort_keys=False, separators=(',', ':'))
        return render(request,'index/debug.html', context)
    return render(request,template, context)
# 自定义api返回值方法
def s(data , res_code = '200'):
    data = {
        'error_code': 0,
        'result':{
            'res_code':res_code,
            'data':data
        }
    }
    return JsonResponse(data)