"""makercity URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls import url
from django.conf import settings
from django.views.static import serve as serve_static  # 处理静态/媒体文件
from index import index
from api import index as api
from api import project as project
from django.views.generic.base import TemplateView

urlpatterns = [
    # url(r'^', include('social_django.urls', namespace='social')), # 微信登录
    path('admin/', admin.site.urls), # admin路由
    url(r'^api/', include('api.urls')),  # api路由
    # path('api/<str:p>/<str:i>/', api.dispose),
    # path('api/<int:id>', project.get_pro_detail),
    path(r'media/<path:path>', serve_static, {'document_root': settings.MEDIA_ROOT}),  # 处理媒体文件
    path(r'static/<path:path>', serve_static, {'document_root': settings.STATIC_ROOT}, ),  # 处理静态文
    path('MP_verify_KM4dCiPt09L3IT9U.txt',TemplateView.as_view(template_name='MP_verify_KM4dCiPt09L3IT9U.txt', content_type='text/plain')),
    url('', include('social_django.urls', namespace='social')),
    path('testhtml/', index.test_html1),
    path('index/', index.index),# 首页
    path('home/', index.home),# 主页
    path('login/', index.login),# 登录
    path('reg/', index.reg),# 注册
    path('edit_project/', index.edit_project),# 编辑项目
    path('project/', index.project),# 编辑项目
    path('get_ajox_token/', api.get_token),# 编辑项目
]
SOCIAL_AUTH_URL_NAMESPACE = 'social'
