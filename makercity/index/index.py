from django.contrib import admin

# Register your models here.
import os
import utils
from django.template import loader # 导入loader方法
from django.shortcuts import render #导入render 方法
from django.http import HttpResponse
from utils import my_render
from config import config

def test_html(request):
    t=loader.get_template('index/test.html')
    html=t.render({'name':'c语言中文网'})#以字典形式传递数据并生成html
    return HttpResponse(html) #以 HttpResponse方式响应html
def test_html1(request):
    return render(request,'index/test.html', {'name':'c语言中文网'})


# 首页
def index(request):
    data = config.all
    # return my_render(request,'index/index.html', {'data':data,'debug':True})
    return render(request,'index/index.html', {'name':'c语言中文网'})
def home(request):
    data = config.all
    # return my_render(request,'index/index.html', {'data':data,'debug':True})
    return render(request,'index/home.html', {'name':'c语言中文网'})
# 登录
def login(request):
    return render(request,'index/login.html')
# 注册
def reg(request):
    return render(request,'index/reg.html')
# 编辑项目
def edit_project(request):
    return render(request,'index/edit_project.html')
# 查看项目
def project(request):
    return render(request,'index/project.html')