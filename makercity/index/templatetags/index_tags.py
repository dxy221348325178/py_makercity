from django import template
import json

register = template.Library()


#注册自定义赋值标签
@register.simple_tag
def test_as_tag(data):
    return json.dumps(data, indent=4,ensure_ascii=False, sort_keys=False,separators=(',', ':'))

@register.filter(name='add_arg')
def template_args(instance, arg):
    """stores the arguments in a separate instance attribute"""
    if not hasattr(instance, "_TemplateArgs"):
        setattr(instance, "_TemplateArgs", [])
    instance._TemplateArgs.append(arg)
    return instance


@register.filter(name='call')
def template_method(instance, method):
    """retrieves the arguments if any and calls the method"""
    method = getattr(instance, method)
    if hasattr(instance, "_TemplateArgs"):
        to_return = method(*instance._TemplateArgs)
        delattr(instance, '_TemplateArgs')
        return to_return
    return method()