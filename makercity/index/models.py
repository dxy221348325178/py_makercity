from django.db import models

# Create your models here.
class User(models.Model):  #创建 用户 表
    id = models.AutoField(verbose_name = '用户表唯一自增ID',primary_key=True)
    nickname = models.CharField(max_length=255, verbose_name='昵称')
    email = models.EmailField(verbose_name='登录邮箱')
    password = models.CharField(max_length=255, verbose_name='登录密码',default='123456')
    sex = models.IntegerField(verbose_name='性别',default=1)
    birth = models.CharField(default="946656001",verbose_name='生日时间戳',max_length=255)
class test(models.Model): # 测试表
    id = models.AutoField(verbose_name = '表唯一自增ID',primary_key=True)
    pro_uid = models.IntegerField(verbose_name='项目作者uid')
    pro_title = models.CharField(default = '',max_length=30, verbose_name='项目标题')
    pro_desc = models.CharField(default = '',max_length = 255,verbose_name = '项目简介')
    pro_home_pic = models.CharField(default = '',max_length = 255,verbose_name = '项目主图')
    pro_zan = models.IntegerField(default=0,verbose_name='项目点赞次数')
    pro_click = models.IntegerField(default=0,verbose_name = '项目浏览次数')
    pro_author = models.IntegerField(default = '',verbose_name = '项目作者')
    pro_datetime = models.CharField(max_length=255, verbose_name='项目创建时间戳')
    pro_edit_datetime = models.CharField(max_length=255, verbose_name='项目修改时间戳')
    pro_status = models.IntegerField(default='',verbose_name = '项目作者')
class project(models.Model): # 测试表
    id = models.AutoField(verbose_name = '表唯一自增ID',primary_key=True)
    pro_uid = models.IntegerField(verbose_name='项目作者uid')
    pro_title = models.CharField(default = '',max_length=30, verbose_name='项目标题')
    pro_desc = models.CharField(default = '',max_length = 255,verbose_name = '项目简介')
    pro_home_pic = models.CharField(default = '',max_length = 255,verbose_name = '项目主图')
    pro_up = models.IntegerField(default=0,verbose_name='项目点赞次数')
    pro_down = models.IntegerField(default=0,verbose_name='项目点踩次数')
    pro_click = models.IntegerField(default=0,verbose_name = '项目浏览次数')
    pro_author = models.IntegerField(default = '',verbose_name = '项目作者')
    pro_datetime = models.CharField(max_length=255, verbose_name='项目创建时间戳')
    pro_edit_datetime = models.CharField(max_length=255, verbose_name='项目修改时间戳')
    pro_detail = models.TextField(default='',verbose_name = '项目详情')
    pro_status = models.IntegerField(default=0,verbose_name = '项目状态')
    # [0:未审核 1:审核通过可以展示 2:审核不通过]
    pro_weight = models.FloatField(default='0.0',verbose_name = '项目权重')
    pro_is_delete = models.IntegerField(default=0,verbose_name = '项目是否删除')
    def dis_fields(self , fields):
        # print(type(self))
        # print(self.pro_detail)
        for index in fields:
            if(hasattr(self, index)):
                print('有'+index)
                print(getattr(self,index))
            else:
                print('无'+index)
        # print(list(self))
        return self.pro_detail