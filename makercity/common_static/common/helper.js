const url = "http://"+window.location.host+'/';
const res_url = "http://res.neuai.net/";// 资源根路径
var myDate = new Date();
/*
* 发送ajax请求
* */
function the_post(api = '',data = {},call_back,err_call_back = function(data){
    that.$message({
        showClose: true,
        message: data.data,
        type: 'error'
    });
},fail_call_back = function(){
    that.$message({
        showClose: true,
        message: '查询失败',
        type: 'error'
    });
}){
    if(api == '')return ;
    var request_url = url + api;
    var is_full_url = false;
    if(data == null){data = {}}
    if($_GET['token']){
        setCookie('token',$_GET['token'])
    }
    if(api.indexOf('http') != -1){
        request_url = api;
        is_full_url = true;
    }else{
        data.token = getCookie('token');
    }
    data.m_id = $_GET['merchant_id'];
    $.get(url + "get_ajox_token/",function(data){
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRFtoken":data.token
            },
            url: request_url,
            data:data,
            success: function(msg){
                if(is_full_url){
                    return call_back(msg);
                }
                if(msg.result.res_code == 'jump_link'){
                    return window.location.href = msg.result.data;
                }
                if(msg.result.res_code == 'loading'){
                    const loading = that.$loading({
                        lock: true,
                        text: 'Loading',
                        spinner: 'el-icon-loading',
                        background: 'rgba(0, 0, 0, 0.7)'
                    });
                }
                if(msg.error_code == 1){
                    if(msg.result.res_code == 301){// 没登录
                        that.$message({
                            showClose: true,
                            message: '请先登录',
                            type: 'error'
                        });
                        to_login();
                    }else if(msg.result.res_code == 302){// 登录了没成为粉丝
                        that.$message({
                            showClose: true,
                            message: msg.result.data,
                            type: 'error'
                        });
                        window.location.href = "/index/index/fans_reg?info="+msg.result.data;
                    }else if(msg.result.res_code == 303){
                        // 还没人工审核
                        window.location.href = "/index/index/show_notice?info="+msg.result.data;
                    }else if(msg.result.res_code == 304){
                        // 人工审核没过
                        window.location.href = "/index/index/show_notice?info="+msg.result.data;
                    }else{
                        err_call_back(msg.result);
                    }
                }
                if(msg.error_code == 300){
                    err_call_back(msg.result);
                    setTimeout(function(){goto_page('login')},1500)
                }
                if(msg.error_code == 0){
                    // console.log(msg.result);
                    call_back(msg.result);
                }
            },
            error:function(msg){
                if(is_full_url){
                    return alert(JSON.stringify(msg));
                }
                fail_call_back(msg);
            }
        });
    })
}
function to_login() {
    return ;
    return window.location.href = '/index/index/login';
    var curl = window.location.href.toString();
    the_post('api/index/get_merchant',{m_id:$_GET['merchant_id']},function (res) {
        var url_ = encodeURIComponent(url+'/api/index/waplogin' + '?mid='+$_GET['merchant_id']+'&up_id='+$_GET['up_id']+'&from=' + encodeURIComponent(curl));
        var jump_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='+res.data.appid+'&redirect_uri=' + url_ + '&response_type=code&scope=snsapi_userinfo&connect_redirect=1#wechat_redirect';
        window.location.href = jump_url;
    })
};
/*
* 获取传参
* */
var $_GET = (function () {
    var url = window.document.location.href.toString();
    var u = url.split("?");
    if (typeof(u[1]) == "string") {
        u = u[1].split("&");
        var get = {};
        for (var i in u) {
            var j = u[i].split("=");
            get[j[0]] = j[1];
        }
        return get;
    } else {
        return {};
    }
})();
/*
* 弹出管理员微信二维码
* */
function alert_ewm(app) {
    app.$alert('<strong>这是 <i>HTML</i> 片段</strong>', 'HTML 片段', {
        dangerouslyUseHTMLString: true
    });
}
/*
* 打印
* */
function log(data){
    console.log(data);
}
/*
* 跳转到个人中心
* */
function goto_mine(){
    window.location.href = 'http://'+window.location.host+'/pages/mine.html';
}
/*
* 页面跳转
* */
function goto_page( page = 'home',data = {}){
    if(page == 'home'){
        window.location.href = 'http://'+window.location.host;
    }
}

//获取cookie、
function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return (arr[2]);
    else
        return null;
}

//设置cookie,增加到vue实例方便全局调用
function setCookie (c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())+";path=/";
};

//删除cookie
function delCookie (name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null)
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString()+";path=/";
};
/*
* 获取当前时间戳*/
function get_timestamp(){
    return Date.parse(new Date()) / 1000;
}
/*
* 时间戳转时间*/
function str2time(timestamp = get_timestamp() , only_hours = true) {
    if(timestamp == null || timestamp == ''){return '';}
    var date=new Date(parseInt(timestamp)* 1000);
    var year = date.getFullYear();
    var mon = date.getMonth()+1 > 9 ? date.getMonth()+1 : '0'+( date.getMonth()+1 );
    var day = date.getDate() > 9 ? date.getDate() : '0'+date.getDate();
    var hours = date.getHours() > 9 ? date.getHours() : '0'+date.getHours();
    var minu = date.getMinutes() > 9 ? date.getMinutes() : '0'+date.getMinutes();
    var sec = date.getSeconds() > 9 ? date.getSeconds() : '0'+date.getSeconds();
    if(only_hours){
        var day = Math.floor( timestamp/ (24*3600) );
        var hour = Math.floor( (timestamp - day*24*3600) / 3600);
        var minute = Math.floor( (timestamp - day*24*3600 - hour*3600) /60 );
        var second = timestamp - day*24*3600 - hour*3600 - minute*60;
        if(hour <= 9){hour = '0'+hour}
        if(minute <= 9){minute = '0'+minute}
        if(second <= 9){second = '0'+second}
        return hour + ":" + minute + ":" + second;
    }
    return year+'-'+mon+'-'+day+' '+hours+':'+minu+':'+sec;
}
/**判断是否是手机号**/
function isPhoneNumber(s) {
    var length = s.length;
    if (length == 11 && /^0?1[3|4|5|6|7|8|9][0-9]\d{8}$/.test(s)) {
        return true;
    } else {
        return false;
    }
}
/*
* 判断是否是邮箱*/
function is_mail(s) {
    if (/^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$/.test(s)) {
        return true;
    } else {
        return false;
    }
}




var height = 0;
var index=1;
var cal=1;

//获取滚动条上下滑动的距离
function getScrollTop(){
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
    if(document.body){
        bodyScrollTop = document.body.scrollTop;
    }
    if(document.documentElement){
        documentScrollTop = document.documentElement.scrollTop;
    }
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
    return scrollTop;
}


//页面的总高度
function getScrollHeight(){
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
    if(document.body){
        bodyScrollHeight = document.body.scrollHeight;
    }
    if(document.documentElement){
        documentScrollHeight = document.documentElement.scrollHeight;
    }
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
    return scrollHeight;
}


//浏览器显示窗口的高度
function getWindowHeight(){
    var windowHeight = 0;
    if(document.compatMode == "CSS1Compat"){
        windowHeight = document.documentElement.clientHeight;
    }else{
        windowHeight = document.body.clientHeight;
    }
    return windowHeight;
}

//重置滚动条的位置到上次滚动的位置
function setOnscroll(height = 0){
    window.scrollTo(0, height);
}

/*
* 字符串转节点
* */
function str2node(str){
    var parent_node = $('<div></div>');
    parent_node.html(str);
    node = parent_node.contents()
    return node;
}

/*
传入数组重新排序,排序功能（arr多维数组，key多维数组键名，order升序或降序）
 */
function sortByKey(arr,key,order){
    for(i=0;i<arr.length;i++){
        for(j=i+1;j<arr.length;j++){
            if(order=='desc'){
                if(parseFloat(arr[i][key])<=parseFloat(arr[j][key])){
                    var min=arr[i];
                    arr[i]=arr[j];
                    arr[j]=min;
                }
            }else{
                if(parseFloat(arr[i][key])>=parseFloat(arr[j][key])){
                    var max=arr[i];
                    arr[i]=arr[j];
                    arr[j]=max;
                }
            }
        }
    }
    return arr;
}

/*
* 操作确认
* 删除级别操作
* */
function del(link,tip) {
    layer.confirm(tip, {icon: 3, title:'删除确认'}, function(index){
        window.location.href=link
        layer.close(index);
    });
}

/*
* 检查是不是空
* */
function not_empty(data) {
    if(data){
        if(typeof data == 'object' && data.length == 0){
            return false;
        }
        return true;
    }
    return false;
}

/*
* 检查是不是手机号
* */
function isphonenumber(s) {
    var length = s.length;
    if (length == 11 && /^0?1[3|4|5|6|7|8][0-9]\d{8}$/.test(s)) {
        return true;
    } else {
        return false;
    }
}
/*
* 提示*/
function el_msg(message = '',type = 'info',seconds=1.5,position = 'bottom-left'){
    that.$message({
        message: message,
        type: type,
        showClose: true,
        duration:seconds * 1000,
        position:position,
    });
}
/*
* 删除数组中指定元素*/
function arr_del(arr, item) {
    var result=[];
    for(var i=0; i<arr.length; i++){
        if(arr[i]!=item){
            result.push(arr[i]);
        }
    }
    return result;
}
/*
* 判断是否存在*/
function empty(data , item = ''){
    if(typeof(data) == 'undefined'){return true;}
}
/*
* 页面跳转*/
function jump_page(link='' , target=''){
    if(target == '_blank'){
        window.open(link)
    }else{
        window.location.href = link;
    }
}